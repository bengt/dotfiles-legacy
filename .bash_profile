# ~/.bash_profile

# Load ~/.bash_aliases, ~/bash_completion and ~/.bash_variables
for file in ~/.{bash_aliases,bash_completion,bash_programs,bash_variables}; do
    [ -r "$file" ] && source "$file"
done
unset file

# Shells

## Color

### set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color) color_prompt=yes;;
esac

### uncomment for a colored prompt, if the terminal has the capability; turned
### off by default to not distract the user: the focus in a terminal window
### should be on the output of commands, not on the prompt
### force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	### We have color support; assume it's compliant with Ecma-48
	### (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	### a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

### If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

## PATH

### check whether the Haskell binary directory exists and if so add it to the PATH
#[ -d "$HOME/Library/Haskell/bin" ] && export PATH="$HOME/Library/Haskell/bin:$PATH";
### check whether the .local binary directory exists and if so add it to the PATH
[ -d "$HOME/.local/bin" ] && export PATH="$HOME/.local/bin:$PATH";
### check whether the mysql binary directory exists and if so add it to the PATH
#[ -d "/usr/local/mysql/bin" ] && export PATH="/usr/local/mysql/bin:$PATH";
### check whether the current user has a ~/bin and if so add it to the PATH
#[ -d "$HOME/bin" ] && export PATH="$HOME/bin:$PATH";
### check whether rvm was installed and if so add it to the PATH
#[ -d "$HOME/.rvm" ] && export PATH="$HOME/.rvm:$PATH";
### remove duplicates from the path
export PATH=`awk -F: '{for(i=1;i<=NF;i++){if(!($i in a)){a[$i];printf s$i;s=":"}}}'<<<$PATH`;


## PROMPT_SMILEY
#export PS1='(\!) \u@\h:\W \[$PROMPT_SMILEY\]\$ ';

## PROMPT_COMMAND
### to be executed every time before a prompt is displayed
#export PROMPT_COMMAND='[ $? -eq 0 ] && PROMPT_SMILEY="" || PROMPT_SMILEY="$(tput setaf 1)! $(tput sgr0)"';

# See https://github.com/jimeh/git-aware-prompt
export DOTBASH=~/.bash/git-aware-prompt
source $DOTBASH/main.sh
export PS1="\u@\h \w\[$txtcyn\]\$git_branch\[$txtrst\]\$ "
export SUDO_PS1="\[$undred\]\u@\h\[$txtrst\] \w\$ "

#PS1='[\u@\h \W$(__git_ps1 " (%s)")]\$ '

## EDITOR

### make `nano` the default system editor
export EDITOR='nano';


## History

### for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=32768
HISTFILESIZE=$HISTSIZE

### don't put duplicate lines in the history. See bash(1) for more options
### ... or force ignoredups and ignorespace
HISTCONTROL=ignoredups:ignorespace

### list of commands that should not be added to history
export HISTIGNORE="exit:[bf]g"

### append to the history file, don't overwrite it
shopt -s histappend
# Append to the Bash history file, rather than overwriting it
#shopt -s histappend


## MANPAGER

## make `man` use use `less` for paging and not clear the screen upon exit
export MANPAGER='less -X';


## Window

### check the window size after each command and, if necessary,
### update the values of LINES and COLUMNS.
shopt -s checkwinsize

## Test
#echo "THIS _IS_ *BASH_PROFILE*"

